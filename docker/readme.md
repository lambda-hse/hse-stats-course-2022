# Запуск докера

Для удобства запуска тетрадок рекомендую использовать докер образ с установленными зависимостями:

## 1. Установка докера

https://docs.docker.com/get-docker/

## 2. Собирание образа

```
docker build --no-cache --platform=linux/amd64 -t  vbelavin/hse_stats_2022 .
```

## 3. Запуск образа

```angular2
docker run -p 8888:8888 -v /Users/vbelavin/hse-stats-course-2022:/root/workdir --platform linux/amd64  vbelavin/hse_stats_2022
```

### NB

- докер образ не работает на Mac с M1 чипом. Все предъявы кидать сюда: [тыц]()